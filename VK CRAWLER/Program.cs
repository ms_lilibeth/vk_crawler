﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VK_CRAWLER.WorkingWithAPI;
using VK_CRAWLER.Model;
using VK_CRAWLER.ExceptionHandlers;

namespace VK_CRAWLER
{
    class Program
    {
        static void Main(string[] args)
        {
            const string ScreenName = "the_brights";
            Publisher publisher = new Publisher();
            VkAPIFacade API_worker = new VkAPIFacade();
            GroupCrawler crawler = new GroupCrawler();
            
            Group theGroup = API_worker.GetGroupObject(ScreenName);
            if (theGroup == null)
                ExceptionHandler.ShowMessage("theGroup == null");
            else
                crawler.Start(API_worker, publisher, theGroup);
        }
        
    }
}
