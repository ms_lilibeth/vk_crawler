﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using VK_CRAWLER.Model;
using VK_CRAWLER.WorkingWithAPI;
using VK_CRAWLER.ExceptionHandlers;

namespace VK_CRAWLER
{
    class GroupCrawler
    {
        const int Max_CanGetMembers = 1000; //Сколько членов группы можно получить за 1 раз. Число берется из документации API
        User_InfoForDB message;
        public void Start(VkAPIFacade API_worker, IPublisher publisher, Group group)
        {
            int TotalCount = 0; //Всего людей в группе            
            int offset = 0; //Смещение при выборе членов группы

            ReadOnlyCollection<long> members = null;
            //За один раз можно получить ограниченное кол-во участников группы. 
            // Поэтому последовательно получается максимально возможное кол-во людей, пока не будут получены все
            do{
                try
                {
                    members = API_worker.GetGroupMembersIDs(group.Id, out TotalCount, Max_CanGetMembers, offset);
                }
                catch (Exception e)
                {
                    ExceptionHandler.ShowMessage("Start: " + e.Message);                    
                }
                User user = null;
                foreach (long memberID in members)
                {
                    user = API_worker.GetUser_byID(memberID);
                    if (user == null){
                        ExceptionHandler.ShowMessage("GroupCrawler: user == null");
                    }        
            
                    message = new User_InfoForDB();
                    message.GROUP_ID = group.Id;
                    message.ID = memberID;
                                        
                    if (user.Country != null)
                    {
                        message.COUNTRY = user.Country.Title;
                    }
                    else message.COUNTRY = "";

                    if (user.City != null)
                    {
                        message.CITY = user.City.Title;
                    }
                    else message.CITY = "";

                    message.AGE = CountAge(user.BirthDate);
                    message.FRIENDS_COUNT = API_worker.CountFriends(memberID);
                    message.ALLPOSTS_COUNT = API_worker.CountAllPosts(memberID);
                    message.REPOSTS_FROMGROUP_COUNT = API_worker.CountRepostsFromGroup(memberID);
                    publisher.SendMessage_Insert(message);
                }
                //Увеличиваем смещение
                offset += members.Count;
            } while (offset < TotalCount);           

        }//скобка от Start

        /// <summary>
        /// Принимает на вход строку формата DD.MM.YYYY или DD.MM, подсчитывает возраст. 
        /// Если пришла строка формата DD.MM, возвращает null, т.к. вычислить возраст невозможно
        /// </summary>
        /// <param name="BirthDate"></param>
        /// <returns> Возраст в годах или null, если вычислить невозможно </returns>
        private short? CountAge(string BirthDate)
        {
            int? result = null;
            if (BirthDate == null)
                return null;
            if (BirthDate.Length <= 5)
                return null;
            string[] tmp = BirthDate.Split(new char[] { '.' });
            foreach (string s in tmp)
            {
                if (s.Length == 4)
                {
                    try
                    {
                        result = Convert.ToInt16(s);
                    }
                    catch (Exception e)
                    {
                        ExceptionHandler.ShowMessage(e.Message);
                        result = null;
                    }
                    break;
                }//скобка от if
            } // скобка от foreach
            return (short?)result;
        }
        
    }
}
