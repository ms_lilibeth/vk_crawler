﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;
using EasyNetQ;
using VK_CRAWLER.Model;
using Newtonsoft.Json;
using Classes_forWorkingWithDB;

namespace VK_CRAWLER
{
    /// <summary>
    /// Связывается с модулем, работающим с БД (в идеале - через RabbitMQ)
    /// </summary>
    public interface IPublisher
    {        
        /// <summary>
        /// Посылает сообщение "Внести в БД информацию о пользователе"
        /// </summary>
        /// <param name="Content"></param>
        void SendMessage_Insert(User_InfoForDB Content);

        /// <summary>
        /// Посылает сообщение "Удалить из БД запись о пользователе со следующим ID"
        /// </summary>
        /// <param name="ID"></param>
        void SendMessage_Delete(long ID);

        /// <summary>
        /// Посылает сообщение "Находится ли пользователь со следующим ID в БД?"
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        bool SendMessage_IsInDatabase(long ID);
    }
    
    class Publisher : IPublisher
    {
        IMessageReceiver SendTo;                
    
        public Publisher()
        {
            SendTo = new MessageReceiver();            
        }

        public void SendMessage_Insert(User_InfoForDB Content)
        {
            SendTo.ReceiveMessage(new VK_CRAWLER.Model.MessageStructure(TypeofMessage.Insert, Content));
        }

        public void SendMessage_Delete(long ID)
        {

        }

        public bool SendMessage_IsInDatabase(long ID)
        {
            return false; //копилирования ради
        }

        /// <summary>
        /// Старая версия; использовалась для отладки
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="API_worker"></param>
        //public void SendMessage(User_InfoRequired Message, VK_CRAWLER.VkAPIFacade API_worker)
        //{
        //    VkNet.VK_CRAWLER.Model.User user = API_worker.GetUser_byID(Message.ID, null);
        //    if (user == null)
        //    {
        //        Console.WriteLine("user == null");
        //    }
        //    else
        //    {
        //        Console.WriteLine(user.Id + " " + user.FirstName + " " + user.LastName);
        //        Console.WriteLine(Message.SEX.ToString()+ " " + Message.AGE + " " + Message.RELATIONSHIP_STATUS.ToString() + " " +
        //            Message.FRIENDS_COUNT + " " + Message.COUNTRY + " " + Message.CITY);
        //    }
        //}

        //------------- Старая версия (провальная попытка работать с EasyNetQ)
        //public void SendMessage_byEasyNetQ(User_InfoRequired Message)
        //{
        //    VK_CRAWLER.Model.MessageStructure theMessageToSend = new VK_CRAWLER.Model.MessageStructure(TypeofMessage.Insert, Message, null);
        //    string serializedMessage = null;
        //    try
        //    {
        //        serializedMessage = JsonConvert.SerializeObject(theMessageToSend);
        //    }
        //    catch (Exception e)
        //    {
        //        VK_CRAWLER.ExceptionHandler.ShowMessage("Ошибка сериализации: " + e.Message);
        //    }
        //    try
        //    {
        //        using (var bus = RabbitHutch.CreateBus("host=localhost"))
        //        {
        //            bus.Publish(new VK_CRAWLER.Model.Message_forBus
        //            {
        //                _SerializedMessage = serializedMessage
        //            });
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        VK_CRAWLER.ExceptionHandler.ShowMessage("Ошибка отправки: " + e.Message);
        //    }
        //    Console.WriteLine("Послано сообщение: " + serializedMessage);
        //}
    }
}
