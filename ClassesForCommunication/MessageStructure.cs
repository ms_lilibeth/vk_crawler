﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_CRAWLER.Model
{
    public enum TypeofMessage { IsInDatabase, Insert, Delete};

    public interface IMessageStructure
    {
        TypeofMessage type
        {
            get;            
        }

        long? userID
        {
            get;            
        }

        User_InfoForDB userInfo
        {
            get;            
        }
    }

    /// <summary>
    /// Сообщение, при помощи которого осуществляется взаимодействие между основным модулем
    /// и модулем, работающим с базой данных
    /// 
    /// </summary>    
    public class MessageStructure
    {
        private TypeofMessage _type;
        private User_InfoForDB _userInfo;
        private long? _userID; // <- планируется использовать для запросов типа IsInDatabase, чтобы не передавать целую структуру User
       
        /// <summary>
        /// Создание сообщения для дальнейшей пересылки ( в идеале - с помощью RabbitMQ). 
        /// <para> Один из параметров userInfo или userID должен быть не null, иначе выбрасывается исключение </para>
        /// 
        /// </summary>
        /// <param name="type"> Тип сообщения </param>
        /// <param name="userInfo"> Структура User_InfoRequired, содержащая подробную информацию о пользователе </param>
        /// <param name="userID"> </param>
        public MessageStructure(TypeofMessage type, User_InfoForDB userInfo)
        {            
            _type = type;
            _userInfo = userInfo;
            _userID = userID;
        }
        public MessageStructure(TypeofMessage type, long userID)
        {
            _type = type;
            _userID = userID;
        }
        public TypeofMessage type{
            get
            {
                return _type;
            }
        }
        public User_InfoForDB userInfo{
            get
            {
                return _userInfo;
            }
        }

        public long? userID
        {
            get
            {
                return _userID;
            }            
        }

    }
}
