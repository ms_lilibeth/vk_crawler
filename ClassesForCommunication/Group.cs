﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_CRAWLER.Model
{
    public enum GroupType
    {
        EVENT, GROUP, PAGE, UNDEFINED
    }
    public class Group
    {
        public City City;
        public Country Country;
        public string Description = "";
        public long Id;
        public string Name = "";
        public string ScreenName = "";
        public GroupType Type;

        public Group()
        {
            City = new City();
            Country = new Country();
            Type = GroupType.UNDEFINED;
        }
        public Group(long id)
        {
            Id = id;
            City = new City();
            Country = new Country();
            Type = GroupType.UNDEFINED;
        }
    }
}
