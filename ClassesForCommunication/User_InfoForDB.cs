﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_CRAWLER.Model
{           
    /// <summary>
    /// Структура, в которой передаются данные о пользователе между модулем, работающим с БД
    /// и другими модулями    
    /// </summary>       
    
    public struct User_InfoForDB
    {
        public long _ID;
        public Sex SEX;
        public short? AGE;
        public RelationshipStatus? RELATIONSHIP_STATUS;
        public string COUNTRY;
        public string CITY;        
        public int? FRIENDS_COUNT;
        public int? ALLPOSTS_COUNT;
        public int? REPOSTS_FROMGROUP_COUNT;
        public long GROUP_ID;
        public long ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
    }
}
