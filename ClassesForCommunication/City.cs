﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_CRAWLER.Model
{
    public class City
    {
        public string Area = "";
        public long? Id = null;
        public bool Important = false;
        public string Region = "";
        public string Title = "";
    }
}
