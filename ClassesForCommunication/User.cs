﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_CRAWLER.Model
{
    /// <summary>
    /// Семейное положение
    /// </summary>
    public enum RelationshipStatus
    {
        UNKNOWN, NOT_MARRIED, HAS_FRIEND, ENGAGED,
        MARRIED, ITS_COMPLEX, IN_ACTIVE_SEARCH, IN_LOVE
    };

    /// <summary>
    /// Пол пользователя
    /// </summary>
    public enum Sex { UNKNOWN, FEMALE, MALE};
       
    public class Education
    {
        public string EducationForm;
        public string EducationStatus;
        public long? FacultyId;
        public string FacultyName;
        public int? Graduation;
        public long? UniversityId;
        public string UniversityName;
    }
    public class User
    {
        public string About;
        public string Activities;
        public string BirthDate;
        public string Books;
        public bool CanSeeAllPosts = false;
        public City City;
        public int FriendsCount = 0;
        public Country Country;
        public string Domain = "";
        public string FirstName = "";
        public string LastName = "";
        public long Id;
        public string Interests = "";
        public RelationshipStatus Relation;
        public Sex Sex;
        public User()
        {
            City = new City();
            Country = new Country();
            Relation = RelationshipStatus.UNKNOWN;
            Sex = Model.Sex.UNKNOWN;
        }
        public User(long id)
        {
            Id = id;
            City = new City();
            Country = new Country();
            Relation = RelationshipStatus.UNKNOWN;
            Sex = Model.Sex.UNKNOWN;
        }
    }
}
