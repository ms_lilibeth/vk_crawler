﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using VK_CRAWLER.WorkingWithAPI;
using VK_CRAWLER.Model;

namespace WorkingWithAPI_tests
{
    class Mappers_tests
    {
        const long ID = 12345;

        VkNet.Model.User VkUserWithIdOnly;
        VkNet.Model.User VkUserWithAllFieldsCompleted;
        User UserWithIdOnly = new User(ID);
        User UserWithAllFieldsCompleted;

        Mappers mapper = new Mappers();

        [Test]
        public void SexMappingTest()
        {
            VkNet.Enums.Sex vkUserSex;
            VK_CRAWLER.Model.Sex UserSex;
            VK_CRAWLER.Model.Sex result;
            //arrange
            vkUserSex = VkNet.Enums.Sex.Unknown;
            UserSex = VK_CRAWLER.Model.Sex.UNKNOWN;
            //act
            result = mapper.SexMapping(vkUserSex);
            //assert
            Assert.AreEqual(UserSex, result);

            //arrange
            vkUserSex = VkNet.Enums.Sex.Male;
            UserSex = VK_CRAWLER.Model.Sex.MALE;
            //act
            result = mapper.SexMapping(vkUserSex);
            //assert
            Assert.AreEqual(UserSex, result);

            //arrange
            vkUserSex = VkNet.Enums.Sex.Female;
            UserSex = VK_CRAWLER.Model.Sex.FEMALE;
            //act
            result = mapper.SexMapping(vkUserSex);
            //assert
            Assert.AreEqual(UserSex, result);
        }
        
        [Test]
        public void RelationMappingTest()
        {
            //arrange
            VK_CRAWLER.Model.RelationshipStatus UserRelation;
            VkNet.Enums.RelationType VkUserRelation;
            VK_CRAWLER.Model.RelationshipStatus result;

            UserRelation = RelationshipStatus.ENGAGED;
            VkUserRelation = VkNet.Enums.RelationType.Engaged;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.HAS_FRIEND;
            VkUserRelation = VkNet.Enums.RelationType.HasFriend;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.IN_ACTIVE_SEARCH;
            VkUserRelation = VkNet.Enums.RelationType.InActiveSearch;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.IN_LOVE;
            VkUserRelation = VkNet.Enums.RelationType.Amorous;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.ITS_COMPLEX;
            VkUserRelation = VkNet.Enums.RelationType.ItsComplex;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.MARRIED;
            VkUserRelation = VkNet.Enums.RelationType.Married;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.NOT_MARRIED;
            VkUserRelation = VkNet.Enums.RelationType.NotMarried;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation, result);

            //arrange
            UserRelation = RelationshipStatus.UNKNOWN;
            VkUserRelation = VkNet.Enums.RelationType.Unknown;
            //act
            result = mapper.RelationTypeMapping(VkUserRelation);
            //assert
            Assert.AreEqual(UserRelation,result);
        }

        private VkNet.Model.User User_CompleteAllFields()
        {
            VkNet.Model.User result = new VkNet.Model.User();
            result.Id = ID;
            result.About = "about";
            result.Activities = "activities";
            result.BirthDate = "01.01.2001";
            result.Books = "books";
            result.CanSeeAllPosts = true;
            result.City.Area = "area";
            result.City.Id = 6789;
            result.City.Important = false;
            result.City.Region = "region";
            result.City.Title = "thecity";
            result.Country.Id = 101112;
            result.Country.Title = "thecountry";
            result.Domain = "the_user";
            result.FirstName = "Ivan";
            //result.FriendsCount = 30;
            result.Interests = "interests";
            result.LastName = "Ivanov";
            result.Relation = VkNet.Enums.RelationType.InActiveSearch;
            result.Sex = VkNet.Enums.Sex.Male;            
            return result;
        }
        //[Test]
        public void MappingUserWithIdOnly()
        {
            //arrange
            User result;
            VkUserWithIdOnly = new VkNet.Model.User();
            VkUserWithIdOnly.Id = ID;
            //act
            result = mapper.UserMapping(VkUserWithIdOnly);
            
            //assert
            Assert.AreEqual(UserWithIdOnly.About, result.About);
            Assert.AreEqual(UserWithIdOnly.Activities, result.Activities);
            Assert.AreEqual(UserWithIdOnly.BirthDate, result.BirthDate);
            Assert.AreEqual(UserWithIdOnly.Books, result.Books);
            Assert.AreEqual(UserWithIdOnly.CanSeeAllPosts, result.CanSeeAllPosts);
            Assert.AreEqual(UserWithIdOnly.City.Area, result.City.Area);
            Assert.AreEqual(UserWithIdOnly.City.Id, result.City.Id);
            Assert.AreEqual(UserWithIdOnly.City.Important, result.City.Important);
            Assert.AreEqual(result.City.Region, result.City.Region);
            Assert.AreEqual(UserWithIdOnly.City.Title, result.City.Title);
            Assert.AreEqual(UserWithIdOnly.Country.Id, result.Country.Id);
            Assert.AreEqual(UserWithIdOnly.Country.Title, result.Country.Title);
            Assert.AreEqual(UserWithIdOnly.Domain, result.Domain);
            Assert.AreEqual(UserWithIdOnly.FirstName, result.FirstName);
            Assert.AreEqual(UserWithIdOnly.FriendsCount, result.FriendsCount);
            Assert.AreEqual(UserWithIdOnly.Id, result.Id);
            Assert.AreEqual(UserWithIdOnly.Interests, result.Interests);
            Assert.AreEqual(UserWithIdOnly.LastName, result.LastName);
            Assert.AreEqual(UserWithIdOnly.Relation, result.Relation);
            Assert.AreEqual(UserWithIdOnly.Sex, result.Sex);           
        }
        //[Test]
        //public void MappingUserWithAllFieldsCompleted(){
        //    //arrange

        //    //act

        //    //assert

        //}
    }
}
