﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using VK_CRAWLER.ExceptionHandlers;

namespace VK_CRAWLER.WorkingWithAPI
{
    public class Mappers
    {        
        public Mappers()
        {
            try
            {
                Mapper.CreateMap<VkNet.Enums.Sex, VK_CRAWLER.Model.Sex>();                    
                Mapper.CreateMap<VkNet.Enums.RelationType, VK_CRAWLER.Model.RelationshipStatus>();
                Mapper.CreateMap<VkNet.Model.Country, VK_CRAWLER.Model.Country>();
                Mapper.CreateMap<VkNet.Model.City, VK_CRAWLER.Model.City>();
                Mapper.CreateMap<VkNet.Model.Education, VK_CRAWLER.Model.Education>();
                Mapper.CreateMap<VkNet.Model.User, VK_CRAWLER.Model.User>();
                Mapper.CreateMap<VkNet.Model.Group, VK_CRAWLER.Model.Group>();
            }
            catch (Exception e)
            {
                ExceptionHandler.ShowMessage("Конструктор Mappers: " + e.Message);
            }
        }

        public VK_CRAWLER.Model.User UserMapping(VkNet.Model.User vkUser)
        {
            return Mapper.Map<VkNet.Model.User, VK_CRAWLER.Model.User>(vkUser);
        }

        public VK_CRAWLER.Model.Group GroupMapping(VkNet.Model.Group vkGroup)
        {
            return Mapper.Map<VkNet.Model.Group, VK_CRAWLER.Model.Group>(vkGroup);
        }

        public VK_CRAWLER.Model.Sex SexMapping(VkNet.Enums.Sex vkUserSex)
        {
            return Mapper.Map<VkNet.Enums.Sex, VK_CRAWLER.Model.Sex>(vkUserSex);
        }

        public VK_CRAWLER.Model.RelationshipStatus RelationTypeMapping(VkNet.Enums.RelationType vkUserRelations)
        {
            return Mapper.Map<VkNet.Enums.RelationType, VK_CRAWLER.Model.RelationshipStatus>(vkUserRelations);
        }
        public VK_CRAWLER.Model.Country CountryMapping(VkNet.Model.Country vkUserCountry)
        {
            return Mapper.Map<VkNet.Model.Country, VK_CRAWLER.Model.Country>(vkUserCountry);
        }
        public VK_CRAWLER.Model.City CityMapping(VkNet.Model.City vkUserCity)
        {
            return Mapper.Map<VkNet.Model.City, VK_CRAWLER.Model.City>(vkUserCity);
        }
        public VK_CRAWLER.Model.Education EducationMapping(VkNet.Model.Education vkUserEducation)
        {
            return Mapper.Map<VkNet.Model.Education, VK_CRAWLER.Model.Education>(vkUserEducation);
        }
    }
}
