﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using VkNet;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Exception;
using VkNet.Enums.Filters;
using AutoMapper;
using VK_CRAWLER.ExceptionHandlers;


namespace VK_CRAWLER.WorkingWithAPI
{
    
    /// <summary>
    /// Инкапсулирует непосредственное обращение к API
    /// Содержит методы для получения информации о пользователе с заданным ID
    /// Отлавливает AccessTokenInvalidException и самостоятельно перелогинивается
    /// </summary>    
    public class VkAPIFacade
    {
        private VkNet.VkApi thisAPI; 
        private NameValueCollection Settings; //настройки, хранящиеся в App.config
        private Mappers Mapper;
        /// <summary>
        /// Получает Access Token по ID приложения, логину и паролю, прописанным в App.config
        /// Возвращает true, если авторизация была успешной, иначе - false.
        /// </summary>
        /// <returns></returns>
        private bool GetAccessToken()
        {
            try
            {
                string AppID = Settings.Get("AppID");
                thisAPI.Authorize(Convert.ToInt32(AppID), Settings.Get("Login"), Settings.Get("Password"), VkNet.Enums.Filters.Settings.Offline);
            }
            catch (Exception e)
            {
                ExceptionHandler.ShowMessage("GetAccessToken: " + e.Message);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Этот конструктор создает новый экземпляр VkApi и получает Access Token
        /// </summary>
        public VkAPIFacade()
        {
            thisAPI = new VkNet.VkApi();            
            Settings = System.Configuration.ConfigurationManager.AppSettings;
            GetAccessToken();  
        }
        /// <summary>
        /// Возвращает экземпляр User по ID
        /// </summary>
        /// <param name="ID"> ID пользователя </param>        
        /// <returns></returns>
        public VK_CRAWLER.Model.User GetUser_byID(long ID){
            User result = null;
            try
            {
                result = thisAPI.Users.Get(ID, ProfileFields.All, null);
            }
            catch (AccessTokenInvalidException)
            {
                //------- Пытаемся заново получить AccessToken                
                if (GetAccessToken())
                {
                    try
                    {
                        result = thisAPI.Users.Get(ID, ProfileFields.All, null);
                    }
                    catch (Exception e)
                    {
                        //Access Token получен, но выходит необработанное исключение при попытке поиска
                        ExceptionHandler.ShowMessage("GetUser_byID" + e.Message);
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.ShowMessage("GetUser_byID" + e.Message);
            }
            return Mapper.UserMapping(result);
        }
         /// <summary>
         /// Получение ID подписчиков группы
         /// </summary>
         /// <param name="groupID"> </param>
         /// <param name="TotalCount"></param>
         /// <param name="HowMany_toGet"></param>
         /// <param name="offset"></param>
         /// <returns></returns>
        public ReadOnlyCollection<long> GetGroupMembersIDs(long groupID, out int TotalCount, int HowMany_toGet, int offset)
        {
            ReadOnlyCollection<long> result = null;
            TotalCount = 0;
            try
            {
                result = thisAPI.Groups.GetMembers(groupID, out TotalCount, HowMany_toGet, offset);
            }
            catch (AccessTokenInvalidException)
            {
                //------- Пытаемся заново получить AccessToken
                if (GetAccessToken())
                {
                    try
                    {
                        result = thisAPI.Groups.GetMembers(groupID, out TotalCount, HowMany_toGet, offset);
                    }
                    catch (Exception e)
                    {
                        //Access Token получен, но выходит необработанное исключение при попытке поиска
                        ExceptionHandler.ShowMessage("GetGroupMembersIDs" + e.Message);                        
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionHandler.ShowMessage("GetGroupMembersIDs" + e.Message);
            }
            return result;
        }
        /// <summary>
        /// Возвращает количество друзей пользователя или null, если доступ запрещен
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public int? CountFriends(long UserID)
        {
            ReadOnlyCollection<User> QueryResult = null;
            try
            {
                QueryResult = thisAPI.Friends.Get(UserID);
            }
            catch (VkNet.Exception.AccessTokenInvalidException) //Недействительный Access Token
            {
                //------- Пытаемся заново получить AccessToken
                
                if (GetAccessToken())
                {
                    try
                    {
                        QueryResult = thisAPI.Friends.Get(UserID);
                    }
                    catch (Exception e)
                    {
                        //Access Token получен, но выходит необработанное исключение при попытке поиска
                        ExceptionHandler.ShowMessage("CountFriends: " + e.Message);
                        return null;
                    }
                }
                else
                {
                    ExceptionHandler.ShowMessage("CountFriends: " + "Дважды не получен Access Token");
                    return null;
                }
            }//скобка от catch (VkNet.Exception.AccessTokenInvalidException)
            catch (AccessDeniedException)
            {
                QueryResult = null;
            }
            catch (Exception e) //Необработанное исключение
            {
                ExceptionHandler.ShowMessage("CountFriends: " + e.Message);
                return null;
            }
            if (QueryResult == null)
            {
                return null;
            }
            else return QueryResult.Count;
        }
        /// <summary>
        /// Возвращает количество всех записей на стене или null, если доступ запрещен
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public int? CountAllPosts(long UserID)
        {
            // TODO: реализовать
            return null;
        }
        /// <summary>
        /// Возвращает количество репостов на стену пользователя из данной группы 
        /// (или null, если доступ запрещен)
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public int? CountRepostsFromGroup(long UserID)
        {
            // TODO: реализовать
            return null;
        }
        
        public VK_CRAWLER.Model.Group GetGroupObject(string ScreenName)
        {
            VkObject ResolveResult = null;
            Group result = null;
            try
            {
                ResolveResult = thisAPI.Utils.ResolveScreenName(ScreenName);
                result = thisAPI.Groups.GetById((long)ResolveResult.Id, null);
            }
            catch (Exception e)
            {
                ExceptionHandler.ShowMessage("GetGroupObject: " + e.Message);
            }
            return Mapper.GroupMapping(result);
        }        
    }
}
