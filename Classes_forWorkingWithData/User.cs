namespace Classes_forWorkingWithDB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public short? Sex { get; set; }

        public short? Age { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        public int? Friends_count { get; set; }

        public int? AllPosts_count { get; set; }

        public int? RepostsFromGroup_count { get; set; }

        public short? Relationship_status { get; set; }

        public long? GroupID { get; set; }
    }
}
