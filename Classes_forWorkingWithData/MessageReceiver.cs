﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VK_CRAWLER.Model;

namespace Classes_forWorkingWithDB
{
    public interface IMessageReceiver
    {
        void ReceiveMessage(MessageStructure MessageReceived);
        //---------- Если будет внедрен EasyNetQ или RabbitMQ:
        //void ReceiveMessage (string Message); 
    }
    public class MessageReceiver : IMessageReceiver
    {
        WorkingWithDB DB_worker;
        public MessageReceiver()
        {
            DB_worker = new WorkingWithDB();
        }
        public void ReceiveMessage(MessageStructure MessageReceived)
        {
            switch (MessageReceived.type)
            {
                case TypeofMessage.Insert:
                    {
                        DB_worker.Insert((User_InfoForDB)MessageReceived.userInfo);
                        break;
                    }
                case TypeofMessage.Delete:
                    {
                        DB_worker.Delete((long)MessageReceived.userID);
                        break;
                    }
            }
        }        

        
    }
}
