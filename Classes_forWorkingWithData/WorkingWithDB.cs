﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VK_CRAWLER.Model;

namespace Classes_forWorkingWithDB
{
    //TODO: нужен ли здесь этот интерфейс?
    //public interface IWorkingWithDB
    //{
    //    void Insert(IUser_InfoRequired userInfo);
    //    void Delete(long ID);
    //    bool IsInDatabase(long ID);
    //}

    public class WorkingWithDB 
    {
        public void Insert(User_InfoForDB info)
        {
            var db = new UserInfoContext();
            var record = new User();
            record.Age = info.AGE;
            record.AllPosts_count = info.ALLPOSTS_COUNT;
            record.City = info.CITY;
            record.Country = info.COUNTRY;
            record.Friends_count = info.FRIENDS_COUNT;
            record.GroupID = info.GROUP_ID;
            record.Id = info.ID;
            record.Relationship_status = (short)info.RELATIONSHIP_STATUS;
            record.RepostsFromGroup_count = info.REPOSTS_FROMGROUP_COUNT;
            record.Sex = (short)info.SEX;
            User SearchResult = db.Users.Find(record.Id);
            if (SearchResult != null)
            {
                db.Users.Remove(SearchResult);
                db.SaveChanges();
            }
            db.Users.Add(record);
            db.SaveChanges();
        }

        public void Delete(long ID)
        {

        }
        public bool IsInDataBase(long ID)
        {

            return false;
        }
    }
}
